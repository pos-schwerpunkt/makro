drop database if exists students;
drop database if exists courses;
drop database if exists bookings;

create database students;
create database courses;
create database bookings;

use students;
select * from student;
select * from hibernate_sequence;

use courses;
select * from course;
select * from hibernate_sequence;

use bookings;
select * from course;
select * from student;
select * from hibernate_sequence;
select * from booking;
select * from student_semester_budget_list;
