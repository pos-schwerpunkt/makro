package at.itkolleg.bookingmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookingmanagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookingmanagementApplication.class, args);
    }

}
