package at.itkolleg.bookingmanagement.api.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateBookingDTO {
    String bookingNumber;
    String flightNumber;
    String passNumber;
    Double price;
    Boolean paid;
}
