package at.itkolleg.bookingmanagement.api.eventlistener;

import at.itkolleg.bookingmanagement.api.mapper.BookingPayedEventToCommand;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotPresentException;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotValidException;
import at.itkolleg.bookingmanagement.infrastructure.messageBroker.BookingChannels;
import at.itkolleg.bookingmanagement.services.BookingPayedCommandService;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BookingChannels.class)
@Slf4j
@AllArgsConstructor
public class BookingChannelListener {
    private final BookingPayedCommandService commandService;

    @StreamListener(target = BookingChannels.BOOKING_CREATION_CHANNEL_SOURCE)
    public void processBookingPayedEvent(BookingPayedEvent event) throws BookingNumberNotPresentException, BookingNumberNotValidException {
        log.info("Listened to Message Broker: Booking payed " + event.getEventData());
        commandService.updatePayment(BookingPayedEventToCommand.toCommand(event));
        log.info("Booking payed: " + event.getEventData());
    }
}
