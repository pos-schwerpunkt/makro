package at.itkolleg.bookingmanagement.api.eventlistener;

import at.itkolleg.bookingmanagement.api.mapper.FlightCreatedEventToCommand;
import at.itkolleg.bookingmanagement.exceptions.FlightNumberDuplicateException;
import at.itkolleg.bookingmanagement.infrastructure.messageBroker.FlightChannels;
import at.itkolleg.bookingmanagement.services.FlightCommandService;
import at.itkolleg.bookingmanagement.shareddomain.events.FlightCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlightChannels.class)
@Slf4j
@AllArgsConstructor
public class FlightChannelListener {
    final FlightCommandService service;

    @StreamListener(target = FlightChannels.FLIGHT_CREATION_CHANNEL_SOURCE)
    public void processFlightCreatedEvent(FlightCreatedEvent event) throws FlightNumberDuplicateException {
        log.info("Listened to Message Broker: Flight Created " + event.getEventData());
        service.createFlight(FlightCreatedEventToCommand.toCommand(event));
        log.info("Flight created: " + event.getEventData());
    }
}
