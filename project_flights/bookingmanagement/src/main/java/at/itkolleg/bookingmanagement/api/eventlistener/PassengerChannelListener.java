package at.itkolleg.bookingmanagement.api.eventlistener;

import at.itkolleg.bookingmanagement.api.mapper.PassengerCreatedEventToCommand;
import at.itkolleg.bookingmanagement.exceptions.DuplicatePassNumberException;
import at.itkolleg.bookingmanagement.exceptions.EmailNotValidException;
import at.itkolleg.bookingmanagement.exceptions.NameNotValidException;
import at.itkolleg.bookingmanagement.exceptions.PassNumberNotValidException;
import at.itkolleg.bookingmanagement.infrastructure.messageBroker.PassengerChannels;
import at.itkolleg.bookingmanagement.services.PassengerCommandService;
import at.itkolleg.bookingmanagement.shareddomain.events.PassengerCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(PassengerChannels.class)
@Slf4j
@AllArgsConstructor
public class PassengerChannelListener {
    final PassengerCommandService service;

    @StreamListener(target = PassengerChannels.PASSENGER_CREATION_CHANNEL_SOURCE)
    public void processPassengerCreatedEvent(PassengerCreatedEvent event) throws NameNotValidException, DuplicatePassNumberException, EmailNotValidException, PassNumberNotValidException {
        log.info("Listened to Message Broker: Passenger created " + event.getEventData());
        service.createPassenger(PassengerCreatedEventToCommand.toCommand(event));
        log.info("Passenger created: " + event.getEventData());
    }
}
