package at.itkolleg.bookingmanagement.api.mapper;

import at.itkolleg.bookingmanagement.domain.booking.command.BookingPayedCommand;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEventData;

public class BookingPayedEventToCommand {

    public static BookingPayedCommand toCommand(BookingPayedEvent event) {
        BookingPayedEventData data = event.getEventData();
        return new BookingPayedCommand(
                data.getPaid(),
                data.getBookingNumber()
        );
    }
}
