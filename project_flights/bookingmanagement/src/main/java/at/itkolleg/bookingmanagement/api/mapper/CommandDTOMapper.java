package at.itkolleg.bookingmanagement.api.mapper;

import at.itkolleg.bookingmanagement.api.dto.CreateBookingDTO;
import at.itkolleg.bookingmanagement.domain.booking.command.CreateBookingCommand;

public class CommandDTOMapper {
    public static CreateBookingCommand toCreateBookingCommand(CreateBookingDTO dto) {
        return new CreateBookingCommand(
                dto.getBookingNumber(),
                dto.getFlightNumber(),
                dto.getPassNumber(),
                dto.getPrice(),
                dto.getPaid()
        );
    }
}
