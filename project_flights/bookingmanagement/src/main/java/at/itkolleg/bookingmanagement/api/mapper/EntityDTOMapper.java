package at.itkolleg.bookingmanagement.api.mapper;

import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.shareddomain.model.BookingResponseDTO;

public class EntityDTOMapper {
    public static BookingResponseDTO toBookingResponseDTO(Booking booking) {
        return new BookingResponseDTO(
                booking.getBookingNumber().getBookingNumber(),
                booking.getFlightNumber(),
                booking.getPassNumber(),
                booking.getPrice().getPrice(),
                booking.getPaid()
        );
    }
}
