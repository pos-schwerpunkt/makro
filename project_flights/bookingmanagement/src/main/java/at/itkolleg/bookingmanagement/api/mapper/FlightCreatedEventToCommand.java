package at.itkolleg.bookingmanagement.api.mapper;

import at.itkolleg.bookingmanagement.domain.flight.CreateFlightCommand;
import at.itkolleg.bookingmanagement.shareddomain.events.FlightCreatedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.FlightCreatedEventData;

public class FlightCreatedEventToCommand {

    public static CreateFlightCommand toCommand(FlightCreatedEvent event) {
        FlightCreatedEventData data = event.getEventData();
        return new CreateFlightCommand(
                data.getFnumber(),
                data.getPNumber(),
                data.getPassenger(),
                data.getFExpStart(),
                data.getFExpLanding(),
                data.getStart(),
                data.getLanding());
    }
}
