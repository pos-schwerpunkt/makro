package at.itkolleg.bookingmanagement.api.mapper;

import at.itkolleg.bookingmanagement.domain.passenger.CreatePassengerCommand;
import at.itkolleg.bookingmanagement.shareddomain.events.PassengerCreatedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.PassengerCreatedEventData;

public class PassengerCreatedEventToCommand {
    public static CreatePassengerCommand toCommand(PassengerCreatedEvent event) {
        PassengerCreatedEventData data = event.getEventData();
        return new CreatePassengerCommand(
                data.getPassNumber(),
                data.getFirstName(),
                data.getLastName(),
                data.getDateOfBirth(),
                data.getEmail()
        );
    }
}
