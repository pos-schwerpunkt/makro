package at.itkolleg.bookingmanagement.api.rest;

import at.itkolleg.bookingmanagement.api.dto.CreateBookingDTO;
import at.itkolleg.bookingmanagement.api.mapper.CommandDTOMapper;
import at.itkolleg.bookingmanagement.api.mapper.EntityDTOMapper;
import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.domain.booking.command.BookingPayedCommand;
import at.itkolleg.bookingmanagement.exceptions.*;
import at.itkolleg.bookingmanagement.services.BookingCommandService;
import at.itkolleg.bookingmanagement.services.BookingPayedCommandService;
import at.itkolleg.bookingmanagement.services.BookingQueryService;
import at.itkolleg.bookingmanagement.shareddomain.model.BookingResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/bookings")
public class BookingController {
    private final BookingCommandService commandService;
    private final BookingPayedCommandService payedCommandService;
    private final BookingQueryService queryService;

    @PostMapping
    public ResponseEntity<Booking> createBooking(@RequestBody CreateBookingDTO bookingDTO)
            throws PassengerLimitException, BookingNumberDuplicateException, FlightNotPresentException, PassNumberNotValidException, BookingNumberNotValidException, FlightNumberNotValidException, PassNumberNotFoundException {
        String bookingNumber = commandService.createBooking(CommandDTOMapper.toCreateBookingCommand(bookingDTO));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{bookingnumber}")
                .buildAndExpand(bookingNumber)
                .toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping
    public List<Booking> getAllBookings() {
        return queryService.getAllBookings();
    }

    @GetMapping("/{bookingnumber}")
    public ResponseEntity<BookingResponseDTO> getBookingByBookingNumber(@PathVariable String bookingnumber) throws
            BookingNumberNotPresentException, BookingNumberNotValidException {
        return new ResponseEntity<>(EntityDTOMapper.toBookingResponseDTO(
                queryService.getBookingByBookingNumber(bookingnumber)
        ), HttpStatus.OK);
    }

    @PostMapping("/payed/")
    public HttpStatus markBookingAsPayed(@RequestBody BookingPayedCommand command) throws BookingNumberNotPresentException, BookingNumberNotValidException {
        payedCommandService.updatePayment(command);
        return HttpStatus.ACCEPTED;
    }

}
