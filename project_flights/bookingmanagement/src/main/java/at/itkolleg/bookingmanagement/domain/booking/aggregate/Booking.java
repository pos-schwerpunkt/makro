package at.itkolleg.bookingmanagement.domain.booking.aggregate;

import at.itkolleg.bookingmanagement.domain.booking.command.BookingPayedCommand;
import at.itkolleg.bookingmanagement.domain.booking.command.CreateBookingCommand;
import at.itkolleg.bookingmanagement.domain.booking.valueObject.BookingNumber;
import at.itkolleg.bookingmanagement.domain.booking.valueObject.Price;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotValidException;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingCreatedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingCreatedEventData;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEventData;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class Booking extends AbstractAggregateRoot<Booking> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    @NotNull
    @Column(unique = true)
    private BookingNumber bookingNumber;

    @NotNull
    private String flightNumber;

    @NotNull
    private String passNumber; // passenger

    @Embedded
    @NotNull
    private Price price;

    private Boolean paid;

    public Booking(CreateBookingCommand command) throws BookingNumberNotValidException {
        this.bookingNumber = new BookingNumber(command.getBookingNumber());
        this.flightNumber = command.getFlightNumber();
        this.passNumber = command.getPassNumber();
        this.price = new Price(command.getPrice());
        this.paid = command.getPaid();

        addDomainEvent(
                new BookingCreatedEvent(
                        new BookingCreatedEventData(
                                this.getBookingNumber().getBookingNumber(),
                                this.getFlightNumber(),
                                this.getPassNumber(),
                                this.getPrice().getPrice(),
                                this.getPaid()
                        )
                )
        );
    }

    public void bookingPayed(BookingPayedCommand command) {
        this.paid = command.getPaid();

        addDomainEvent(
                new BookingPayedEvent(
                        new BookingPayedEventData(
                                this.getPaid(),
                                this.getBookingNumber().getBookingNumber()
                        )
                )
        );
    }


    private void addDomainEvent(Object event) {
        registerEvent(event);
    }
}
