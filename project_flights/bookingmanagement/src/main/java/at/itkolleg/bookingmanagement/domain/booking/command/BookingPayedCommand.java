package at.itkolleg.bookingmanagement.domain.booking.command;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class BookingPayedCommand {
    Boolean paid;
    String bookingNumber;
}
