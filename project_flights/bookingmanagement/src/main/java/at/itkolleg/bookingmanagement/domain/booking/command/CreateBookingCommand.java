package at.itkolleg.bookingmanagement.domain.booking.command;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreateBookingCommand {
    String bookingNumber;
    String flightNumber;
    String passNumber;
    Double price;
    Boolean paid;
}
