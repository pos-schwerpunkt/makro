package at.itkolleg.bookingmanagement.domain.booking.valueObject;

import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class BookingNumber {
    private String bookingNumber;

    protected BookingNumber() {}

    public BookingNumber(String bookingNumber) throws BookingNumberNotValidException {
        if (bookingNumber.matches("[A-Z]{2}-[0-9]{5}[a-z]{2}")) {
            this.bookingNumber = bookingNumber;
        } else {
            throw new BookingNumberNotValidException();
        }

    }
}
