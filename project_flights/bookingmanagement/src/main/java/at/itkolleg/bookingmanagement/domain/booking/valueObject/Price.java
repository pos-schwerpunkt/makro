package at.itkolleg.bookingmanagement.domain.booking.valueObject;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@Getter
@EqualsAndHashCode
public class Price {

    @NotNull
    private double price;

    protected Price() {}

    public Price(Double price)  {
        if(price >= 0) {
            this.price = price;
        }
    }
}
