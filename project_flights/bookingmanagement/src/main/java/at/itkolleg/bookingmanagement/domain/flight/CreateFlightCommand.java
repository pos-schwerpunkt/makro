package at.itkolleg.bookingmanagement.domain.flight;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Timestamp;

@Value
@AllArgsConstructor
public class CreateFlightCommand {
    String fnumber;
    String pNumber;
    Integer passenger;
    Timestamp fExpStart;
    Timestamp fExpLanding;
    String start;
    String landing;
}
