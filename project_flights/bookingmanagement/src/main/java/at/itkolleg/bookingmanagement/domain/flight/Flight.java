package at.itkolleg.bookingmanagement.domain.flight;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Flight extends AbstractAggregateRoot<Flight> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long fId;

    private String fnumber;

    private String pNumber; // foreign key for plane
    private Integer passenger;

    private Timestamp fExpStart;
    private Timestamp fExpLanding;

    private String start; // foreign key for IACO-start-airport
    private String landing; // foreign key for IACO-landing-airport

    public Flight(CreateFlightCommand command) {
        this.fnumber = command.getFnumber();
        this.pNumber = command.getPNumber();
        this.passenger = command.getPassenger();
        this.fExpStart = command.getFExpStart();
        this.fExpLanding = command.getFExpLanding();
        this.start = command.getStart();
        this.landing = command.getLanding();
    }
}
