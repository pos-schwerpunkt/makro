package at.itkolleg.bookingmanagement.domain.passenger;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Date;

@Value
@AllArgsConstructor
public class CreatePassengerCommand {
    String passNumber;
    String firstName;
    String lastName;
    Date dateOfBirth;
    String email;
}
