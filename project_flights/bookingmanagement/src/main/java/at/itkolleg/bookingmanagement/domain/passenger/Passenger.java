package at.itkolleg.bookingmanagement.domain.passenger;


import at.itkolleg.bookingmanagement.exceptions.EmailNotValidException;
import at.itkolleg.bookingmanagement.exceptions.NameNotValidException;
import at.itkolleg.bookingmanagement.exceptions.PassNumberNotValidException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Entity
@NoArgsConstructor
@Getter
public class Passenger extends AbstractAggregateRoot<Passenger> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String passNumber;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private Date dateOfBirth;

    @NotNull
    private String email;

    public Passenger(CreatePassengerCommand command) throws PassNumberNotValidException, NameNotValidException, EmailNotValidException {
        this.passNumber = command.getPassNumber();
        this.firstName = command.getFirstName();
        this.lastName = command.getLastName();
        this.dateOfBirth = command.getDateOfBirth();
        this.email = command.getEmail();
    }

}
