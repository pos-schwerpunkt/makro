package at.itkolleg.bookingmanagement.exceptions;

public class BookingNumberDuplicateException extends Exception {
    public BookingNumberDuplicateException() {
        super("Booking number already in use.");
    }
}
