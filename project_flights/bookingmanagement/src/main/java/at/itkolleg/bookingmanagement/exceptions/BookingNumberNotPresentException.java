package at.itkolleg.bookingmanagement.exceptions;

public class BookingNumberNotPresentException extends Exception {
    public BookingNumberNotPresentException() {
        super("Required booking number is not present.");
    }
}
