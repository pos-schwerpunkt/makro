package at.itkolleg.bookingmanagement.exceptions;

public class BookingNumberNotValidException extends Exception {
    public BookingNumberNotValidException() {
        super("Booking number not valid. Sample: XX-00000xx");
    }
}
