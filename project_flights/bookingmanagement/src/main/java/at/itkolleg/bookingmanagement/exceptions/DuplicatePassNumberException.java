package at.itkolleg.bookingmanagement.exceptions;

public class DuplicatePassNumberException extends Exception {
    public DuplicatePassNumberException() {
        super("Pass number already in use. No duplicates allowed!");
    }
}
