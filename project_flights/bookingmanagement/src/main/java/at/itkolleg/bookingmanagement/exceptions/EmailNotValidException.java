package at.itkolleg.bookingmanagement.exceptions;

public class EmailNotValidException extends Exception {
    public EmailNotValidException() {
        super("Given mail-address not valid.");
    }
}
