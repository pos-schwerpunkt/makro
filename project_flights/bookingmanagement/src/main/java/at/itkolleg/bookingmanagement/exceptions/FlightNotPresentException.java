package at.itkolleg.bookingmanagement.exceptions;

public class FlightNotPresentException extends Exception {
    public FlightNotPresentException() {
        super("The requested flight is not available.");
    }
}
