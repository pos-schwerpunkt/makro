package at.itkolleg.bookingmanagement.exceptions;

public class FlightNumberDuplicateException extends Exception {
    public FlightNumberDuplicateException() {
        super("Flightnumber already in use.");
    }
}
