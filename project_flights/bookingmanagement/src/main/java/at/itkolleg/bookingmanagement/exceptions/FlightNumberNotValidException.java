package at.itkolleg.bookingmanagement.exceptions;

public class FlightNumberNotValidException extends Exception {
    public FlightNumberNotValidException() {
        super("FlightNumber not valid!");
    }
}
