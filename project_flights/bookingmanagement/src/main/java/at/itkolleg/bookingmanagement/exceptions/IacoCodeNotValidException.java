package at.itkolleg.bookingmanagement.exceptions;

public class IacoCodeNotValidException extends Exception { // if IACO-Code is fetched from database
    public IacoCodeNotValidException() {
        super("Provided IACO-Code is not valid.");
    }
}
