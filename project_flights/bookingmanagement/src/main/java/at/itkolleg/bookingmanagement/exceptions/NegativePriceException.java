package at.itkolleg.bookingmanagement.exceptions;

public class NegativePriceException extends Exception {
    public NegativePriceException() {
        super("Minimum price must be 0 (zero).");
    }
}
