package at.itkolleg.bookingmanagement.exceptions;

public class PassNumberNotFoundException extends Exception {
    public PassNumberNotFoundException() {
        super("Required pass number is not present.");
    }
}
