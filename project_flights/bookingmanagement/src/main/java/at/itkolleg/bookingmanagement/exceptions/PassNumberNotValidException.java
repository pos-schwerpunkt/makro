package at.itkolleg.bookingmanagement.exceptions;

public class PassNumberNotValidException extends Exception {
    public PassNumberNotValidException() {
        super("Pass not valid. Must contain 7 digits, no letters.");
    }
}
