package at.itkolleg.bookingmanagement.exceptions;

public class PassengerLimitException extends Exception {
    public PassengerLimitException() {
        super("Plane is full, no more passengers possible. Sorry!");
    }
}
