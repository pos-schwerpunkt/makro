package at.itkolleg.bookingmanagement.exceptions;

public class WrongParameterException extends Exception {
    public WrongParameterException() {
        super("Wrong parameter, chose 0 for not paid or 1 for paid.");
    }
}
