package at.itkolleg.bookingmanagement.infrastructure.messageBroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface BookingChannels {

    String BOOKING_CREATION_CHANNEL_SOURCE = "bookingCreationChannelSource";
    String BOOKING_CREATION_CHANNEL_SINK = "bookingCreationChannelSink";

    @Output(BOOKING_CREATION_CHANNEL_SOURCE)
    MessageChannel bookingCreationSource();

    @Input(BOOKING_CREATION_CHANNEL_SINK)
    MessageChannel bookingCreationSink();
}
