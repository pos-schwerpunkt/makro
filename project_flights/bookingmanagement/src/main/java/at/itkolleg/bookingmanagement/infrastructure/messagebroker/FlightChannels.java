package at.itkolleg.bookingmanagement.infrastructure.messageBroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface FlightChannels {

    String FLIGHT_CREATION_CHANNEL_SOURCE = "flightCreationChannelSource";
    String FLIGHT_CREATION_CHANNEL_SINK = "flightCreationChannelSink";

    @Input(FLIGHT_CREATION_CHANNEL_SOURCE)
    MessageChannel flightCreationSink();
}
