package at.itkolleg.bookingmanagement.infrastructure.messageBroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface PassengerChannels {

    String PASSENGER_CREATION_CHANNEL_SOURCE = "passengerCreationChannelSource";

    @Input(PASSENGER_CREATION_CHANNEL_SOURCE)
    MessageChannel passengerCreationSource();
}
