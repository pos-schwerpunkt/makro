package at.itkolleg.bookingmanagement.infrastructure.repositories;

import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.domain.booking.valueObject.BookingNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long> {
    Optional<Booking> getBookingByBookingNumber(BookingNumber number);
    List<Booking> getBookingByFlightNumber(String number);
    List<Booking> getBookingByPaid(boolean cond);
    void deleteBookingByBookingNumber(BookingNumber bookingNumber);
}
