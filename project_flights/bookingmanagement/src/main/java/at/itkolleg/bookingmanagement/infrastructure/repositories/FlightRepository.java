package at.itkolleg.bookingmanagement.infrastructure.repositories;

import at.itkolleg.bookingmanagement.domain.flight.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
    Optional<Flight> findFlightByFnumber(String fnumber);
}
