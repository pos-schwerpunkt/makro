package at.itkolleg.bookingmanagement.infrastructure.repositories;

import at.itkolleg.bookingmanagement.domain.passenger.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    Optional<Passenger> findPassengerByPassNumber(String number);
}
