package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.domain.booking.command.CreateBookingCommand;
import at.itkolleg.bookingmanagement.domain.flight.Flight;
import at.itkolleg.bookingmanagement.exceptions.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class BookingCommandService {
    private final Double OVERBOOKING = 1.1;
    private final BookingQueryService bookingQueryService;
    private final FlightQueryService flightQueryService;
    private final PassengerQueryService passengerQueryService;

    public String createBooking(CreateBookingCommand command)
            throws FlightNumberNotValidException, BookingNumberDuplicateException, PassengerLimitException, FlightNotPresentException, PassNumberNotValidException, BookingNumberNotValidException, PassNumberNotFoundException {
        Booking booking = new Booking(command);

        if (bookingQueryService.BookingNumberInUse(booking.getBookingNumber().getBookingNumber())) {
            throw new BookingNumberDuplicateException(); // booking-number already in use
        }

        if (!flightQueryService.existsFlightWithNumber(command.getFlightNumber())) {
            throw new FlightNotPresentException(); // flight for booking not found in local database
        }

        if (!passengerQueryService.PassNumberInUse(command.getPassNumber())) {
            throw new PassNumberNotFoundException();
        }

        Flight flight = flightQueryService.getFlightByNumber(command.getFlightNumber());
        double maxPassenger = flight.getPassenger() * OVERBOOKING; // 10% Überbuchung möglich
        int currentPassenger = 1 + bookingQueryService.getBookingsByFlightNumber(command.getFlightNumber());
        if(currentPassenger > maxPassenger) {
            log.error("Too many passengers! possible: " + maxPassenger + ", current passengers: " + currentPassenger);
            throw new PassengerLimitException();
        }
        log.info("Current passengers for this flight " + currentPassenger + ", max. available passenger: " + maxPassenger);
        bookingQueryService.saveBooking(booking); // booking-number is new and flight exists
        return booking.getBookingNumber().getBookingNumber();
    }
}
