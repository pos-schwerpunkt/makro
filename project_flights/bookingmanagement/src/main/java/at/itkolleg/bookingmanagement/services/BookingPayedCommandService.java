package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.domain.booking.command.BookingPayedCommand;
import at.itkolleg.bookingmanagement.domain.booking.valueObject.BookingNumber;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotPresentException;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotValidException;
import at.itkolleg.bookingmanagement.infrastructure.repositories.BookingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class BookingPayedCommandService {
    private final BookingRepository repo;

    public void updatePayment(BookingPayedCommand command) throws BookingNumberNotPresentException, BookingNumberNotValidException {
        Optional<Booking> optionalBooking = repo.getBookingByBookingNumber(new BookingNumber(command.getBookingNumber()));
        if (optionalBooking.isPresent()) {
            Booking booking = optionalBooking.get();
            booking.bookingPayed(command);
            repo.save(booking);
        } else {
            throw new BookingNumberNotPresentException();
        }
    }
}

// not working due to "no provider found"
//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("booking");
//        EntityManager em = emf.createEntityManager();
//
//        Booking booking = queryService.getBookingByBookingNumber(bookingNumber);
//        Long id = booking.getId();
//
//        Booking b = em.find(Booking.class, id);
//        b.setPaid(true);
//
//        em.getTransaction().begin();
//        em.merge(b);
//        em.getTransaction().commit();
//
//
//        // speichern
//        em.getTransaction().begin();
//        em.persist(b);
//        em.getTransaction().commit();
