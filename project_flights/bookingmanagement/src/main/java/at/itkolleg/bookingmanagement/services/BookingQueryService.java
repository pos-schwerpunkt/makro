package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.domain.booking.aggregate.Booking;
import at.itkolleg.bookingmanagement.domain.booking.valueObject.BookingNumber;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotPresentException;
import at.itkolleg.bookingmanagement.exceptions.BookingNumberNotValidException;
import at.itkolleg.bookingmanagement.exceptions.FlightNumberNotValidException;
import at.itkolleg.bookingmanagement.exceptions.WrongParameterException;
import at.itkolleg.bookingmanagement.infrastructure.repositories.BookingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BookingQueryService {
    private final BookingRepository repo;

    public Boolean BookingNumberInUse(String bookingNumber) throws BookingNumberNotValidException {
        return repo.getBookingByBookingNumber(new BookingNumber(bookingNumber)).isPresent();
    }

    public Booking getBookingByBookingNumber(String bookingNumber) throws BookingNumberNotPresentException, BookingNumberNotValidException {
        Optional<Booking> optionalBooking = repo.getBookingByBookingNumber(new BookingNumber(bookingNumber));
        if(optionalBooking.isPresent()) {
            return optionalBooking.get();
        } else {
            throw new BookingNumberNotPresentException();
        }
    }
    public int getBookingsByFlightNumber(String flightnumber) throws FlightNumberNotValidException {
        return (repo.getBookingByFlightNumber(flightnumber).size());
    }

    public List<Booking> getPaidBookings(int status) throws WrongParameterException {
        switch (status) {
            case 0: return repo.getBookingByPaid(false); // return all bookings which are not paid yet
            case 1: return repo.getBookingByPaid(true); // return all bookings which are paid
            default: throw new WrongParameterException();
        }

    }

    public List<Booking> getAllBookings() {
        return repo.findAll();
    }

    public void saveBooking(Booking booking) {
        repo.save(booking);
    }

    public void deleteBookingByBookingNumber(String number) throws BookingNumberNotValidException {
        repo.deleteBookingByBookingNumber(new BookingNumber(number));
    }
}
