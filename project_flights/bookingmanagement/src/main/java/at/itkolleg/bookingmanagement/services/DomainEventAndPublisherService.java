package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.infrastructure.messageBroker.BookingChannels;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingCreatedEvent;
import at.itkolleg.bookingmanagement.shareddomain.events.BookingPayedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@AllArgsConstructor
@Slf4j
@EnableBinding(BookingChannels.class)
public class DomainEventAndPublisherService {
    private final BookingChannels bookingChannels;

    @TransactionalEventListener
    public void handleBookingCreatedEvent(BookingCreatedEvent event) {
        try {
            log.info("Listened to Domain Event: Booking created: " + event.getEventData());
            log.info("Sending domain event to Message Broker Queue ... ");
            bookingChannels.bookingCreationSource().send(MessageBuilder.withPayload(event).build());
            log.info("Event has been sent successfully.");
        } catch (Exception e) {
            log.error("Custom Error: No connection to Message Broker established! Data not send.");
            e.printStackTrace();
        }
    }

    @TransactionalEventListener
    public void handleBookingPayedEvent(BookingPayedEvent event) {
        try {
            log.info("Listened to Domain Event: Booking payed: " + event.getEventData());
            log.info("Sending domain event to Message Broker Queue ... ");
            bookingChannels.bookingCreationSource().send(MessageBuilder.withPayload(event).build());
            log.info("Event has been sent successfully.");
        } catch (Exception e) {
            log.error("Custom Error: No connection to Message Broker established! Data not send.");
            e.printStackTrace();
        }
    }
}
