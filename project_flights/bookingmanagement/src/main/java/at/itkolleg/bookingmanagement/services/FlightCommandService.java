package at.itkolleg.bookingmanagement.services;


import at.itkolleg.bookingmanagement.domain.flight.CreateFlightCommand;
import at.itkolleg.bookingmanagement.domain.flight.Flight;
import at.itkolleg.bookingmanagement.exceptions.FlightNumberDuplicateException;
import at.itkolleg.bookingmanagement.infrastructure.repositories.FlightRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FlightCommandService {

    private final FlightRepository repo;

    public void createFlight(CreateFlightCommand command) throws FlightNumberDuplicateException {
        if (repo.findFlightByFnumber(command.getFnumber()).isEmpty()) {
            Flight flight = new Flight(command);
            repo.save(flight);
        } else {
            throw new FlightNumberDuplicateException();
        }
    }
}
