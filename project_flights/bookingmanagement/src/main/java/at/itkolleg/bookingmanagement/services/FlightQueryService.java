package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.domain.flight.Flight;
import at.itkolleg.bookingmanagement.exceptions.FlightNotPresentException;
import at.itkolleg.bookingmanagement.infrastructure.repositories.FlightRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class FlightQueryService {
    private final FlightRepository repo;

    public Boolean existsFlightWithNumber(String flightnumber) {
        return repo.findFlightByFnumber(flightnumber).isPresent();
    }

    public Flight getFlightByNumber(String flightnumber) throws FlightNotPresentException {
        Optional<Flight> optionalFlight = repo.findFlightByFnumber(flightnumber);
        if (optionalFlight.isPresent()) {
            return optionalFlight.get();
        } else {
            throw new FlightNotPresentException();
        }
    }
}
