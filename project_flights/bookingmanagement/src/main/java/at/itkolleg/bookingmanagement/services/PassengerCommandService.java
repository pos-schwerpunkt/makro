package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.domain.passenger.CreatePassengerCommand;
import at.itkolleg.bookingmanagement.domain.passenger.Passenger;
import at.itkolleg.bookingmanagement.exceptions.DuplicatePassNumberException;
import at.itkolleg.bookingmanagement.exceptions.EmailNotValidException;
import at.itkolleg.bookingmanagement.exceptions.NameNotValidException;
import at.itkolleg.bookingmanagement.exceptions.PassNumberNotValidException;
import at.itkolleg.bookingmanagement.infrastructure.repositories.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PassengerCommandService {
    private final PassengerRepository repo;
    private final PassengerQueryService queryService;

    public void createPassenger(CreatePassengerCommand command) throws EmailNotValidException, NameNotValidException, PassNumberNotValidException, DuplicatePassNumberException {
        Passenger passenger = new Passenger(command);
        if (queryService.PassNumberInUse(command.getPassNumber())) {
            throw new DuplicatePassNumberException();
        } else {
            repo.save(passenger);
        }
    }
}
