package at.itkolleg.bookingmanagement.services;

import at.itkolleg.bookingmanagement.infrastructure.repositories.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class PassengerQueryService {
    private final PassengerRepository repo;

    public boolean PassNumberInUse(String pNumber) {
        return repo.findPassengerByPassNumber(pNumber).isPresent();
    }
}
