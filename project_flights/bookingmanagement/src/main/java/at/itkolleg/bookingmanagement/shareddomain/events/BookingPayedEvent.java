package at.itkolleg.bookingmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingPayedEvent {
    private BookingPayedEventData eventData;
}
