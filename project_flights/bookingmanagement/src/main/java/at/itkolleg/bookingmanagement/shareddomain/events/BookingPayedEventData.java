package at.itkolleg.bookingmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingPayedEventData {
    private Boolean paid;
    private String bookingNumber;
}
