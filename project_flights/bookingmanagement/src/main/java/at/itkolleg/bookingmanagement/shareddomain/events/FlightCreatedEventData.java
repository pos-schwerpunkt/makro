package at.itkolleg.bookingmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightCreatedEventData {
    private String fnumber;
    private String pNumber;
    private Integer passenger;
    private Timestamp fExpStart;
    private Timestamp fExpLanding;
    private String start;
    private String landing;
}
