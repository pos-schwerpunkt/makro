package at.itkolleg.bookingmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;

@Data
@AllArgsConstructor
public class PassengerCreatedEventData {
    private String passNumber;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String email;
}
