package at.itkolleg.bookingmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlaneCreatedEventData {
    private String planeNumber;
    private String manufacturer;
    private String planeType;
    private Integer maxSeats;
    private Double maxRange;
}
