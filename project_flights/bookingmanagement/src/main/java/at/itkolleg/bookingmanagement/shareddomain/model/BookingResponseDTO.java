package at.itkolleg.bookingmanagement.shareddomain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingResponseDTO {
    private String bookingNumber;
    private String flightNumber;
    private String passNumber;
    private Double price;
    private Boolean paid;
}
