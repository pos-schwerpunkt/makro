package at.itkolleg.flightmanagement.api;

import at.itkolleg.flightmanagement.api.dto.CreateFlightDTO;
import at.itkolleg.flightmanagement.api.mappers.CommandDTOMapper;
import at.itkolleg.flightmanagement.api.mappers.EntityDTOMapper;
import at.itkolleg.flightmanagement.domain.flight.aggregate.Flight;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.IACO;
import at.itkolleg.flightmanagement.exceptions.*;
import at.itkolleg.flightmanagement.services.FlightCommandService;
import at.itkolleg.flightmanagement.services.FlightQueryService;
import at.itkolleg.flightmanagement.shareddomain.model.FlightResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/flights")
public class FlightController {

    private final FlightCommandService commandService;
    private final FlightQueryService queryService;

    @PostMapping
    public ResponseEntity<Flight> createFlight(@RequestBody CreateFlightDTO dto) throws FlightNumberNotValidException, FlightNumberNotPresentException, IacoCodeNotValidException, FlightNumberDuplicateException, NegativePassengerException, PlaneNumberNotPresentException {
        String flightNumber = commandService.createFlight(CommandDTOMapper.toCreateFlightCommand(dto));
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{flightnumber}")
                .buildAndExpand(flightNumber)
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @GetMapping
    public List<Flight> showAllFlights() {
        return queryService.getAllFlights();
    }

    @GetMapping("/{flightnumber}")
    public ResponseEntity<FlightResponseDTO> getFlightByNumber(@PathVariable String flightnumber) throws FlightNumberNotPresentException, FlightNumberNotValidException {
        return new ResponseEntity<>(EntityDTOMapper.toFlightResponseDTO(
                queryService.getFlightByFNumber(flightnumber)
        ), HttpStatus.OK);
    }

    @GetMapping("/starts/{iaco}")
    public List<Flight> showAllStartsByIACO(@PathVariable String iaco) throws IacoCodeNotValidException {
        return queryService.getAllStartsByIACO(new IACO(iaco));
    }

    @GetMapping("/landings/{iaco}")
    public List<Flight> showAllLandingsByIACO(@PathVariable String iaco) throws IacoCodeNotValidException {
        return queryService.getAllLandingsByIACO(new IACO(iaco));
    }
}
