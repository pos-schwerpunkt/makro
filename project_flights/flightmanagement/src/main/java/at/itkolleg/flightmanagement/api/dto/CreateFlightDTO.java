package at.itkolleg.flightmanagement.api.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Timestamp;

@Value
// @Value annotation is short hand for the annotation combination
// @Getter
// @FieldDefaults(makeFinal=true, level= AccessLevel.PRIVATE)
// @AllArgsConstructor
// @ToString
// @EqualsAndHashCode .
@AllArgsConstructor
public class CreateFlightDTO {
    String fnumber;
    String pNumber;
    Integer passenger;
    Timestamp fExpStart;
    Timestamp fExpLanding;
    String start;
    String landing;
}
