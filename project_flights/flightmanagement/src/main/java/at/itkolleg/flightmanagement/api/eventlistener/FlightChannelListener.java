package at.itkolleg.flightmanagement.api.eventlistener;

import at.itkolleg.flightmanagement.infrastructure.messagebroker.FlightChannels;
import at.itkolleg.flightmanagement.shareddomain.events.FlightCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(FlightChannels.class)
@Slf4j
public class FlightChannelListener {
    @StreamListener(target = FlightChannels.FLIGHT_CREATION_CHANNEL_SINK)
    public void processFlightCreated(FlightCreatedEvent event) {
        log.info("Listened to Message Broker: Flight Event = " + event);
    }
}
