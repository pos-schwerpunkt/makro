package at.itkolleg.flightmanagement.api.eventlistener;

import at.itkolleg.flightmanagement.api.mappers.PlaneCreatedEventToCommand;
import at.itkolleg.flightmanagement.exceptions.DuplicatePlaneNumberException;
import at.itkolleg.flightmanagement.infrastructure.messagebroker.PlaneChannels;
import at.itkolleg.flightmanagement.services.PlaneCommandService;
import at.itkolleg.flightmanagement.shareddomain.events.PlaneCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(PlaneChannels.class)
@Slf4j
@AllArgsConstructor
public class PlaneChannelListener {
    final PlaneCommandService commandService;

    @StreamListener(target = PlaneChannels.PLANE_CREATION_CHANNEL_SOURCE)
    public void processPlaneCreationEvent(PlaneCreatedEvent event) throws DuplicatePlaneNumberException {
        log.info("Listened to Message Broker: Plane created -> " + event.getEventData());
        commandService.createPlane(PlaneCreatedEventToCommand.toCommand(event));
        log.info("Plane created: " + event.getEventData());
    }
}
