package at.itkolleg.flightmanagement.api.mappers;

import at.itkolleg.flightmanagement.api.dto.CreateFlightDTO;
import at.itkolleg.flightmanagement.domain.flight.commands.CreateFlightCommand;

public class CommandDTOMapper {

    public static CreateFlightDTO toCreateFlightDTO(CreateFlightCommand command) {
        return new CreateFlightDTO(
                command.getFnumber(),
                command.getPNumber(),
                command.getPassenger(),
                command.getFExpStart(),
                command.getFExpLanding(),
                command.getStart(),
                command.getLanding()
        );
    }

    public static CreateFlightCommand toCreateFlightCommand(CreateFlightDTO dto) {
        return new CreateFlightCommand(
                dto.getFnumber(),
                dto.getPNumber(),
                dto.getPassenger(),
                dto.getFExpStart(),
                dto.getFExpLanding(),
                dto.getStart(),
                dto.getLanding()
        );
    }
}
