package at.itkolleg.flightmanagement.api.mappers;

import at.itkolleg.flightmanagement.domain.flight.aggregate.Flight;
import at.itkolleg.flightmanagement.shareddomain.model.FlightResponseDTO;

public class EntityDTOMapper {
    public static FlightResponseDTO toFlightResponseDTO(Flight flight) {
        return new FlightResponseDTO(
                flight.getFlightNumber().getFlightNumber(),
                flight.getPlaneNumber(),
                flight.getPassenger().getPassenger(),
                flight.getFExpStart(),
                flight.getFExpLanding(),
                flight.getStart().getICode(),
                flight.getLanding().getICode()
        );
    }
}
