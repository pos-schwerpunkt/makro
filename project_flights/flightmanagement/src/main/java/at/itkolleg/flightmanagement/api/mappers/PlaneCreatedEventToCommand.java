package at.itkolleg.flightmanagement.api.mappers;

import at.itkolleg.flightmanagement.domain.plane.CreatePlaneCommand;
import at.itkolleg.flightmanagement.shareddomain.events.PlaneCreatedEvent;
import at.itkolleg.flightmanagement.shareddomain.events.PlaneCreatedEventData;

public class PlaneCreatedEventToCommand {
    public static CreatePlaneCommand toCommand(PlaneCreatedEvent event) {
        PlaneCreatedEventData data = event.getEventData();
        return new CreatePlaneCommand(
                data.getPlaneNumber(),
                data.getManufacturer(),
                data.getPlaneType(),
                data.getMaxSeats(),
                data.getMaxRange()
        );
    }
}
