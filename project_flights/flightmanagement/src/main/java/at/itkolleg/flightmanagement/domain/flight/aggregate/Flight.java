package at.itkolleg.flightmanagement.domain.flight.aggregate;

import at.itkolleg.flightmanagement.domain.flight.commands.CreateFlightCommand;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.FlightNumber;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.IACO;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.Passenger;
import at.itkolleg.flightmanagement.exceptions.FlightNumberNotValidException;
import at.itkolleg.flightmanagement.exceptions.IacoCodeNotValidException;
import at.itkolleg.flightmanagement.exceptions.NegativePassengerException;
import at.itkolleg.flightmanagement.shareddomain.events.FlightCreatedEvent;
import at.itkolleg.flightmanagement.shareddomain.events.FlightCreatedEventData;
import lombok.*;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class Flight extends AbstractAggregateRoot<Flight> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    @Column(unique = true)
    private FlightNumber flightNumber;

    private String planeNumber; // foreign key for plane
    private Passenger passenger;

    private Timestamp fExpStart;
    private Timestamp fExpLanding;

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "iCode", column = @Column(name = "start"))
    })
    private IACO start; // foreign key for IACO-start-airport

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "iCode", column = @Column(name = "landing"))
    })
    private IACO landing; // foreign key for IACO-landing-airport

    public Flight(CreateFlightCommand flightCommand) throws FlightNumberNotValidException, IacoCodeNotValidException, NegativePassengerException {
        this.flightNumber = new FlightNumber(flightCommand.getFnumber());
        this.planeNumber = flightCommand.getPNumber();
        this.passenger = new Passenger(flightCommand.getPassenger());
        this.fExpStart = flightCommand.getFExpStart();
        this.fExpLanding = flightCommand.getFExpLanding();
        this.start = new IACO(flightCommand.getStart());
        this.landing = new IACO(flightCommand.getLanding());

        addDomainEvent(
                new FlightCreatedEvent(
                        new FlightCreatedEventData(
                                this.flightNumber.getFlightNumber(),
                                this.planeNumber,
                                this.passenger.getPassenger(),
                                this.fExpStart,
                                this.fExpLanding,
                                this.start.getICode(),
                                this.landing.getICode()
                        )
                )
        );
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

}
