package at.itkolleg.flightmanagement.domain.flight.valueObjects;

import at.itkolleg.flightmanagement.exceptions.FlightNumberNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class FlightNumber {

    @NotNull
    @Size(min = 5, max = 10)
    @Column(unique = true)
    private String flightNumber;

    protected FlightNumber() {}

    public FlightNumber(String flightNumber) throws FlightNumberNotValidException {
        if(flightNumber.matches("[0-9]{5,10}[A-Z]")) {
            this.flightNumber = flightNumber;
        } else {
            throw new FlightNumberNotValidException();
        }
    }
}
