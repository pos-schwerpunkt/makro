package at.itkolleg.flightmanagement.domain.flight.valueObjects;

import at.itkolleg.flightmanagement.exceptions.IacoCodeNotValidException;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Data
@EqualsAndHashCode
public class IACO {

    @Column(unique = true)
    @Size(min = 4, max = 5)
    @NotNull
    private String iCode = "ABDC";

    protected IACO() {}

    public IACO(String code) throws IacoCodeNotValidException {
        if(code != null && code.length() >= 4 && code.length() <= 5) {
            this.iCode = code;
        } else {
            throw new IacoCodeNotValidException();
        }
    }


}
