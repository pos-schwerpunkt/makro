package at.itkolleg.flightmanagement.domain.flight.valueObjects;

import at.itkolleg.flightmanagement.exceptions.NegativePassengerException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
@Getter
@EqualsAndHashCode
public class Passenger {

    @NotNull
    @Size(min = 0)
    private Integer passenger;

    protected Passenger() {}

    public Passenger(int number) throws NegativePassengerException {
        if(number >= 0) {
            this.passenger = number;
        } else {
            throw new NegativePassengerException();
        }
    }
}
