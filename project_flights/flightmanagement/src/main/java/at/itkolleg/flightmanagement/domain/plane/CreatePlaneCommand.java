package at.itkolleg.flightmanagement.domain.plane;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreatePlaneCommand {
    String planeNumber;
    String manufacturer;
    String planeType;
    Integer maxSeats;
    Double maxRange;
}
