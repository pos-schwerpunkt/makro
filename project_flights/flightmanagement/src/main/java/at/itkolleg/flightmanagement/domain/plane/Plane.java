package at.itkolleg.flightmanagement.domain.plane;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class Plane extends AbstractAggregateRoot<Plane> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pId;

    @Column(unique = true)
    @NotNull
    private String planeNumber;

    @NotNull
    private String manufacturer;

    @NotNull
    private String planeType;

    @NotNull
    private int maxSeats;

    @NotNull
    private double maxRange;

    public Plane(CreatePlaneCommand command) {
        this.planeNumber = command.getPlaneNumber();
        this.manufacturer = command.getManufacturer();
        this.planeType = command.getPlaneType();
        this.maxSeats = command.getMaxSeats();
        this.maxRange = command.getMaxRange();
    }
}
