package at.itkolleg.flightmanagement.exceptions;

public class DuplicatePlaneNumberException extends Exception {
    public DuplicatePlaneNumberException() {
        super("Planenumber already in use!");
    }
}
