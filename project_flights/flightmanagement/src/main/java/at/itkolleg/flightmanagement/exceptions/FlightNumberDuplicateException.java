package at.itkolleg.flightmanagement.exceptions;

public class FlightNumberDuplicateException extends Exception {
    public FlightNumberDuplicateException() {
        super("Flightnumber already in use.");
    }
}
