package at.itkolleg.flightmanagement.exceptions;

public class FlightNumberNotPresentException extends Exception {
    public FlightNumberNotPresentException() {
        super("Requested flight-number not found in system.");
    }
}
