package at.itkolleg.flightmanagement.exceptions;

public class FlightNumberNotValidException extends Exception {
    public FlightNumberNotValidException() {
        super("FlightNumber must not contain letters!");
    }
}
