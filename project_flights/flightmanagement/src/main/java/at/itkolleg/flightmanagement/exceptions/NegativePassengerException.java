package at.itkolleg.flightmanagement.exceptions;

public class NegativePassengerException extends Exception {
    public NegativePassengerException() {
        super("Passenger-number cannot be negative.");
    }
}
