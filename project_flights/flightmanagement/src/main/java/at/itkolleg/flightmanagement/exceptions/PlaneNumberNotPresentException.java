package at.itkolleg.flightmanagement.exceptions;

public class PlaneNumberNotPresentException extends Exception {
    public PlaneNumberNotPresentException() {
        super("Requested plane number is not present.");
    }
}
