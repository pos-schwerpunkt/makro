package at.itkolleg.flightmanagement.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface FlightChannels {
    String FLIGHT_CREATION_CHANNEL_SOURCE = "flightCreationChannelSource";
    String FLIGHT_CREATION_CHANNEL_SINK = "flightCreationChannelSink";

    @Output(FLIGHT_CREATION_CHANNEL_SOURCE)
    MessageChannel flightCreationSource();

    @Input(FLIGHT_CREATION_CHANNEL_SINK)
    MessageChannel flightCreationSink();
}
