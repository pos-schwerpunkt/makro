package at.itkolleg.flightmanagement.infrastructure.messagebroker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.MessageChannel;

public interface PlaneChannels {
    String PLANE_CREATION_CHANNEL_SOURCE = "planeCreationChannelSource";

    @Input(PLANE_CREATION_CHANNEL_SOURCE)
    MessageChannel planeCreationSource();
}
