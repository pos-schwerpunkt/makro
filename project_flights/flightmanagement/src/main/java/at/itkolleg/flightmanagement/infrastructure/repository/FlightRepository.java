package at.itkolleg.flightmanagement.infrastructure.repository;

import at.itkolleg.flightmanagement.domain.flight.aggregate.Flight;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.FlightNumber;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.IACO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
    Optional<Flight> findFlightByFlightNumber(FlightNumber fnumber);
    List<Flight> findFlightByStart(IACO start);
    List<Flight> findFlightByLanding(IACO landing);
    void deleteFlightByFlightNumber(FlightNumber flightNumber);
}
