package at.itkolleg.flightmanagement.infrastructure.repository;

import at.itkolleg.flightmanagement.domain.plane.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlaneRepository extends JpaRepository<Plane, Long> {
    Optional<Plane> findPlaneByPlaneNumber(String number);
    void deletePlanesByPlaneNumber(String number);
}
