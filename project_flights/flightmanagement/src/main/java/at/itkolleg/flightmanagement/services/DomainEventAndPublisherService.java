package at.itkolleg.flightmanagement.services;

import at.itkolleg.flightmanagement.infrastructure.messagebroker.FlightChannels;
import at.itkolleg.flightmanagement.shareddomain.events.FlightCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@EnableBinding(FlightChannels.class)
@Slf4j
@AllArgsConstructor
public class DomainEventAndPublisherService {
    private final FlightChannels flightChannels;

    @TransactionalEventListener
    public void handleFlightCreatedEvent(FlightCreatedEvent event) {
        try{
            log.info("Listend to Domain Event: Flight created" + event.getEventData());
            log.info("Send Domain Event Data to Message Broker Queue");
            flightChannels.flightCreationSource().send(MessageBuilder.withPayload(event).build());
            log.info("Event has been sent: " + event.getEventData());
        } catch (Exception e) {
            log.error("Problem: No connection to Message Broker established! Data not send: "  + event.getEventData());
            e.printStackTrace();
        }
    }
}
