package at.itkolleg.flightmanagement.services;


import at.itkolleg.flightmanagement.domain.flight.aggregate.Flight;
import at.itkolleg.flightmanagement.domain.flight.commands.CreateFlightCommand;
import at.itkolleg.flightmanagement.exceptions.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FlightCommandService {

    private final FlightQueryService flightQueryService;
    private final PlaneQueryService planeQueryService;

    public String createFlight(CreateFlightCommand createFlightCommand) throws IacoCodeNotValidException, FlightNumberDuplicateException, FlightNumberNotValidException, NegativePassengerException, PlaneNumberNotPresentException {
        Flight flight = new Flight(createFlightCommand);

        if (flightQueryService.flightNumberInUse(flight.getFlightNumber().getFlightNumber())) {
            throw new FlightNumberDuplicateException(); // flight number already in use
        }

        if (!planeQueryService.PlaneNumberExists(flight.getPlaneNumber())) {
            throw new PlaneNumberNotPresentException(); // plane number does not exist in local database
        }

        if(planeQueryService.getNumberOfPassengers(flight.getPlaneNumber()) < 1) {
            throw new NegativePassengerException(); // capacity min. 1 passenger
        }

        flightQueryService.saveFlight(flight);
        return flight.getFlightNumber().getFlightNumber();

    }
}
