package at.itkolleg.flightmanagement.services;


import at.itkolleg.flightmanagement.domain.flight.aggregate.Flight;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.FlightNumber;
import at.itkolleg.flightmanagement.domain.flight.valueObjects.IACO;
import at.itkolleg.flightmanagement.exceptions.FlightNumberNotPresentException;
import at.itkolleg.flightmanagement.exceptions.FlightNumberNotValidException;
import at.itkolleg.flightmanagement.infrastructure.repository.FlightRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class FlightQueryService {
    private final FlightRepository repo;

    public boolean flightNumberInUse(String fnumber) throws FlightNumberNotValidException {
        return repo.findFlightByFlightNumber(new FlightNumber(fnumber)).isPresent();
    }

    public Flight getFlightByFNumber(String fnumber) throws FlightNumberNotPresentException, FlightNumberNotValidException {
        Optional<Flight> optionalFlight = repo.findFlightByFlightNumber(new FlightNumber(fnumber));
        if(optionalFlight.isPresent()) {
            return optionalFlight.get();
        } else {
            throw new FlightNumberNotPresentException();
        }
    }

    public List<Flight> getAllStartsByIACO(IACO iaco) {
        return repo.findFlightByStart(iaco);
    }

    public List<Flight> getAllLandingsByIACO(IACO iaco) {
        return repo.findFlightByLanding(iaco);
    }

    public List<Flight> getAllFlights() {
        return repo.findAll();
    }

    public void saveFlight(Flight flight) {
        repo.save(flight);
    }

    public void deleteFlightByNumber(String flightNumber) throws FlightNumberNotValidException {
        repo.deleteFlightByFlightNumber(new FlightNumber(flightNumber));
    }


}
