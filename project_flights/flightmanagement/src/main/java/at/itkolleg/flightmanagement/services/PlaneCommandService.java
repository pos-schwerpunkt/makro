package at.itkolleg.flightmanagement.services;


import at.itkolleg.flightmanagement.domain.plane.CreatePlaneCommand;
import at.itkolleg.flightmanagement.domain.plane.Plane;
import at.itkolleg.flightmanagement.exceptions.DuplicatePlaneNumberException;
import at.itkolleg.flightmanagement.exceptions.PlaneNumberNotPresentException;
import at.itkolleg.flightmanagement.infrastructure.repository.PlaneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PlaneCommandService {

    private final PlaneRepository repo;

    public String createPlane(CreatePlaneCommand command) throws DuplicatePlaneNumberException {
        Plane plane = new Plane(command);
        if(PlaneNumberInUse(plane.getPlaneNumber())) {
            throw new DuplicatePlaneNumberException();
        } else {
            repo.save(plane);
            return plane.getPlaneNumber();
        }
    }

    public void deletePlane(String pNumber) throws PlaneNumberNotPresentException {
        if(PlaneNumberInUse(pNumber)) {
            repo.deletePlanesByPlaneNumber(pNumber);
        } else {
            throw new PlaneNumberNotPresentException();
        }
    }
    private boolean PlaneNumberInUse(String pNumber) {
        return repo.findPlaneByPlaneNumber(pNumber).isPresent();
    }
}
