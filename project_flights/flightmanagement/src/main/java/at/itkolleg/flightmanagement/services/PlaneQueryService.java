package at.itkolleg.flightmanagement.services;

import at.itkolleg.flightmanagement.domain.plane.Plane;
import at.itkolleg.flightmanagement.exceptions.PlaneNumberNotPresentException;
import at.itkolleg.flightmanagement.infrastructure.repository.PlaneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PlaneQueryService {
    private final PlaneRepository repo;

    public boolean PlaneNumberExists(String planeNumber) {
        return repo.findPlaneByPlaneNumber(planeNumber).isPresent();
    }

    public int getNumberOfPassengers(String planeNumber) throws PlaneNumberNotPresentException {
        Optional<Plane> optionalPlane = repo.findPlaneByPlaneNumber(planeNumber);
        if (optionalPlane.isPresent()) {
            Plane plane = optionalPlane.get();
            return plane.getMaxSeats();
        } else {
            throw new PlaneNumberNotPresentException();
        }
    }

}
