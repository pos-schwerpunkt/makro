package at.itkolleg.flightmanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaneCreatedEvent {
    private PlaneCreatedEventData eventData;
}
