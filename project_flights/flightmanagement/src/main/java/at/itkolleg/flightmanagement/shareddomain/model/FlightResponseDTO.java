package at.itkolleg.flightmanagement.shareddomain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightResponseDTO {
    private String fnumber;
    private String pNumber;
    private Integer passenger;
    private Timestamp fExpStart;
    private Timestamp fExpLanding;
    private String iStart;
    private String iLanding;
}
