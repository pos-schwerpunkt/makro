package at.itkolleg.passengermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassengermanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassengermanagementApplication.class, args);
	}

}
