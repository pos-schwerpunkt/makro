package at.itkolleg.passengermanagement.api;

import at.itkolleg.passengermanagement.api.dto.CreatePassengerDTO;
import at.itkolleg.passengermanagement.api.mappers.CommandDTOMapper;
import at.itkolleg.passengermanagement.api.mappers.EntityDTOMapper;
import at.itkolleg.passengermanagement.domain.aggregate.Passenger;
import at.itkolleg.passengermanagement.exceptions.*;
import at.itkolleg.passengermanagement.services.CommandService;
import at.itkolleg.passengermanagement.services.QueryService;
import at.itkolleg.passengermanagement.shareddomain.model.PassengerResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/passengers")
public class PassengerController {

    private final CommandService commandService;
    private final QueryService queyService;

    @PostMapping
    public ResponseEntity<Passenger> createPassenger(@RequestBody CreatePassengerDTO passengerDTO) throws NameNotValidException, DuplicatePassNumberException, EmailNotValidException, PassNumberNotValidException {
        String passNumber = commandService.createPassenger(CommandDTOMapper.toCreatePassengerCommand(passengerDTO));
        URI path = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{passnumber}")
                .buildAndExpand(passNumber)
                .toUri();
        return ResponseEntity.created(path).build();
    }

    @GetMapping
    public List<Passenger> getAllPassengers() {
        return queyService.getAllPassengers();
    }

    @GetMapping("/{passnumber}")
    public ResponseEntity<PassengerResponseDTO> getPassengerByPassNumber(@PathVariable String passnumber) throws PassNumberNotValidException, PassNumberNotPresentException {
        return new ResponseEntity<>(EntityDTOMapper.toPassengerResponseDTO(
                queyService.getPassengerByPassNumber(passnumber)
        ), HttpStatus.OK);
    }

}
