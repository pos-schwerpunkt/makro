package at.itkolleg.passengermanagement.api.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.sql.Date;

@Value
@AllArgsConstructor
public class CreatePassengerDTO {
    String passNumber;
    String firstName;
    String lastName;
    Date dateOfBirth;
    String email;
}
