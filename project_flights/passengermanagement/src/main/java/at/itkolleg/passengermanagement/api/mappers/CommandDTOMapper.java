package at.itkolleg.passengermanagement.api.mappers;

import at.itkolleg.passengermanagement.api.dto.CreatePassengerDTO;
import at.itkolleg.passengermanagement.domain.commands.CreatePassengerCommand;

public class CommandDTOMapper {
    public static CreatePassengerCommand toCreatePassengerCommand(CreatePassengerDTO dto) {
        return new CreatePassengerCommand(
                dto.getPassNumber(),
                dto.getFirstName(),
                dto.getLastName(),
                dto.getDateOfBirth(),
                dto.getEmail()
        );
    }
}
