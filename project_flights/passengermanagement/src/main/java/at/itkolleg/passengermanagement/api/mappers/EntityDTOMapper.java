package at.itkolleg.passengermanagement.api.mappers;

import at.itkolleg.passengermanagement.domain.aggregate.Passenger;
import at.itkolleg.passengermanagement.shareddomain.model.PassengerResponseDTO;

public class EntityDTOMapper {
    public static PassengerResponseDTO toPassengerResponseDTO(Passenger passenger) {
        return new PassengerResponseDTO(
                passenger.getPassNumber().getPassNumber(),
                passenger.getFirstName().getName(),
                passenger.getLastName().getName(),
                passenger.getDateOfBirth(),
                passenger.getEmail().getMail()
        );
    }
}
