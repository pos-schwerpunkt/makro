package at.itkolleg.passengermanagement.domain.aggregate;

import at.itkolleg.passengermanagement.domain.commands.CreatePassengerCommand;
import at.itkolleg.passengermanagement.domain.valueObjects.Email;
import at.itkolleg.passengermanagement.domain.valueObjects.PassNumber;
import at.itkolleg.passengermanagement.domain.valueObjects.PersonName;
import at.itkolleg.passengermanagement.exceptions.EmailNotValidException;
import at.itkolleg.passengermanagement.exceptions.NameNotValidException;
import at.itkolleg.passengermanagement.exceptions.PassNumberNotValidException;
import at.itkolleg.passengermanagement.shareddomain.events.PassengerCreatedEvent;
import at.itkolleg.passengermanagement.shareddomain.events.PassengerCreatedEventData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Entity
@NoArgsConstructor
@Getter
public class Passenger extends AbstractAggregateRoot<Passenger> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @NotNull
    @Column(unique = true)
    private PassNumber passNumber;

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "firstName"))
    })
    private PersonName firstName;

    @Embedded
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "name", column = @Column(name = "lastName"))
    })
    private PersonName lastName;

    private Date dateOfBirth;

    @Embedded
    @NotNull
    private Email email;

    public Passenger(CreatePassengerCommand command) throws PassNumberNotValidException, NameNotValidException, EmailNotValidException {
        this.passNumber = new PassNumber(command.getPassNumber());
        this.firstName = new PersonName(command.getFirstName());
        this.lastName = new PersonName(command.getLastName());
        this.dateOfBirth = command.getDateOfBirth();
        this.email = new Email(command.getEmail());

        addDomainEvent(
                new PassengerCreatedEvent(
                        new PassengerCreatedEventData(
                                this.passNumber.getPassNumber(),
                                this.firstName.getName(),
                                this.lastName.getName(),
                                this.dateOfBirth,
                                this.email.getMail()
                        )
                )
        );
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }

}
