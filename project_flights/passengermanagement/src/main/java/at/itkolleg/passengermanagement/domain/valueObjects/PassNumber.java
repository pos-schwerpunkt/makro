package at.itkolleg.passengermanagement.domain.valueObjects;

import at.itkolleg.passengermanagement.exceptions.PassNumberNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class PassNumber {
    private String passNumber;

    protected PassNumber() {}

    public PassNumber(String number) throws PassNumberNotValidException {
        if (number.matches("^[A-Z][0-9]{7}")) {
            this.passNumber = number;
        } else {
            throw new PassNumberNotValidException();
        }
    }
}
