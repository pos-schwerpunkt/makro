package at.itkolleg.passengermanagement.domain.valueObjects;

import at.itkolleg.passengermanagement.exceptions.NameNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class PersonName {
    private String name;

    protected PersonName() {}

    public PersonName(String name) throws NameNotValidException {
        if (name.matches("[A-z]{2,10}")) {
            this.name = name;
        } else {
            throw new NameNotValidException();
        }
    }
}
