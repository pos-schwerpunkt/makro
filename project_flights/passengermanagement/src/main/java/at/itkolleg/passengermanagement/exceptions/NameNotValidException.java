package at.itkolleg.passengermanagement.exceptions;

public class NameNotValidException extends Exception {
    public NameNotValidException() {
        super("Name is not valid! Min 2 and max 10 characters.");
    }
}
