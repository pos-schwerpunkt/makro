package at.itkolleg.passengermanagement.exceptions;

public class PassNumberNotPresentException extends Exception {
    public PassNumberNotPresentException() {
        super("Requested pass number not found in system.");
    }
}
