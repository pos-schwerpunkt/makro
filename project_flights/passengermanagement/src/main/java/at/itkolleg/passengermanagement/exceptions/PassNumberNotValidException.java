package at.itkolleg.passengermanagement.exceptions;

public class PassNumberNotValidException extends Exception {
    public PassNumberNotValidException() {
        super("Pass not valid. Must contain 7 digits, no letters.");
    }
}
