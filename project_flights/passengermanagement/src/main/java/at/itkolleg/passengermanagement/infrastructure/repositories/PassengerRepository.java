package at.itkolleg.passengermanagement.infrastructure.repositories;

import at.itkolleg.passengermanagement.domain.aggregate.Passenger;
import at.itkolleg.passengermanagement.domain.valueObjects.PassNumber;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
    Optional<Passenger> findPassengerByPassNumber(PassNumber number);
}
