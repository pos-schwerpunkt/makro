package at.itkolleg.passengermanagement.services;

import at.itkolleg.passengermanagement.domain.aggregate.Passenger;
import at.itkolleg.passengermanagement.domain.commands.CreatePassengerCommand;
import at.itkolleg.passengermanagement.exceptions.DuplicatePassNumberException;
import at.itkolleg.passengermanagement.exceptions.EmailNotValidException;
import at.itkolleg.passengermanagement.exceptions.NameNotValidException;
import at.itkolleg.passengermanagement.exceptions.PassNumberNotValidException;
import at.itkolleg.passengermanagement.infrastructure.repositories.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommandService {
    private final PassengerRepository repo;
    private final QueryService queyService;

    public String createPassenger(CreatePassengerCommand command) throws EmailNotValidException, NameNotValidException, PassNumberNotValidException, DuplicatePassNumberException {
        Passenger passenger = new Passenger(command);
        if (queyService.PassNumberInUse(command.getPassNumber())) {
            throw new DuplicatePassNumberException();
        } else {
            repo.save(passenger);
            return passenger.getPassNumber().getPassNumber();
        }
    }
}
