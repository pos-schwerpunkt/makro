package at.itkolleg.passengermanagement.services;

import at.itkolleg.passengermanagement.infrastructure.messageBroker.PassengerChannels;
import at.itkolleg.passengermanagement.shareddomain.events.PassengerCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@AllArgsConstructor
@Slf4j
@EnableBinding(PassengerChannels.class)
public class DomainEventAndPublisherService {
    private final PassengerChannels passengerChannels;

    @TransactionalEventListener
    public void handlePassengerCreatedEvent(PassengerCreatedEvent event) {
        try {
            log.info("Listened to Domain Event: Passenger created: " + event.getEventData());
            log.info("Sending domain event to Message Broker Queue ... ");
            passengerChannels.planeCreationSource().send(MessageBuilder.withPayload(event).build());
            log.info("Event has been sent successfully. :)");
        } catch (Exception e) {
            log.error("Custom Error: No connection to Message Broker established! Data not send.");
            e.printStackTrace();
        }
    }
}
