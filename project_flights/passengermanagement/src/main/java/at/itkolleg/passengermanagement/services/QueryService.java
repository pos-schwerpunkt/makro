package at.itkolleg.passengermanagement.services;

import at.itkolleg.passengermanagement.domain.aggregate.Passenger;
import at.itkolleg.passengermanagement.domain.valueObjects.PassNumber;
import at.itkolleg.passengermanagement.exceptions.PassNumberNotPresentException;
import at.itkolleg.passengermanagement.exceptions.PassNumberNotValidException;
import at.itkolleg.passengermanagement.infrastructure.repositories.PassengerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class QueryService {
    private final PassengerRepository repo;

    public boolean PassNumberInUse(String pNumber) throws PassNumberNotValidException {
        return repo.findPassengerByPassNumber(new PassNumber(pNumber)).isPresent();
    }

    public Passenger getPassengerByPassNumber(String passNumber) throws PassNumberNotValidException, PassNumberNotPresentException {
        Optional<Passenger> optionalPassenger = repo.findPassengerByPassNumber(new PassNumber(passNumber));
        if (optionalPassenger.isPresent()) {
            return optionalPassenger.get();
        } else {
            throw new PassNumberNotPresentException();
        }
    }

    public List<Passenger> getAllPassengers() {
        return repo.findAll();
    }
}
