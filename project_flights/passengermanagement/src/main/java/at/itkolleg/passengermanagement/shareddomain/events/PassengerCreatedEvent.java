package at.itkolleg.passengermanagement.shareddomain.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassengerCreatedEvent {
    private PassengerCreatedEventData eventData;
}
