package at.itkolleg.passengermanagement.shareddomain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassengerResponseDTO {
    private String passNumber;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String email;
}
