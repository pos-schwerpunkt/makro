package at.itkolleg.planemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanemanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlanemanagementApplication.class, args);
	}

}
