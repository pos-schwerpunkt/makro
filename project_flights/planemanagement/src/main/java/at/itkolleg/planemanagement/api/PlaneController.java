package at.itkolleg.planemanagement.api;

import at.itkolleg.planemanagement.api.dto.CreatePlaneDTO;
import at.itkolleg.planemanagement.api.mappers.CommandDTOMapper;
import at.itkolleg.planemanagement.api.mappers.EntityDTOMapper;
import at.itkolleg.planemanagement.domain.aggregate.Plane;
import at.itkolleg.planemanagement.exceptions.DuplicatePlaneNumberException;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotPresentException;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotValidException;
import at.itkolleg.planemanagement.services.CommandService;
import at.itkolleg.planemanagement.services.QueryService;
import at.itkolleg.planemanagement.shareddomain.model.PlaneResponseDTO;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/planes")
public class PlaneController {

    private final CommandService commandService;
    private final QueryService queryService;

    @PostMapping
    public ResponseEntity<Plane> createPlane(@RequestBody CreatePlaneDTO createPlaneDTO) throws DuplicatePlaneNumberException, PlaneNumberNotValidException {
        String planeNumber = commandService.createPlane(CommandDTOMapper.toCreatePlaneCommand(createPlaneDTO));
        URI path = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{planenumber}")
                .buildAndExpand(planeNumber)
                .toUri();
        return ResponseEntity.created(path).build();
    }

    @GetMapping
    public List<Plane> getAllPlanes() {
        return queryService.getAllPlanes();
    }

    @GetMapping("/{planenumber}")
    public ResponseEntity<PlaneResponseDTO> getPlaneByPNumber(@PathVariable String planenumber) throws PlaneNumberNotPresentException, PlaneNumberNotValidException {
        return new ResponseEntity<>(EntityDTOMapper.toPlaneResponseDTO(
                queryService.getPlaneByPNumber(planenumber)
        ), HttpStatus.OK);
    }
}
