package at.itkolleg.planemanagement.api.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class CreatePlaneDTO {
    String planeNumber;
    String manufacturer;
    String planeType;
    Integer maxSeats;
    Double maxRange;
}
