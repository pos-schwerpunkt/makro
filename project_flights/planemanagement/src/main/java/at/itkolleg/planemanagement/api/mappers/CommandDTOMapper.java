package at.itkolleg.planemanagement.api.mappers;

import at.itkolleg.planemanagement.api.dto.CreatePlaneDTO;
import at.itkolleg.planemanagement.domain.commands.CreatePlaneCommand;

public class CommandDTOMapper {

    public static CreatePlaneCommand toCreatePlaneCommand(CreatePlaneDTO planeDTO) {
        return new CreatePlaneCommand(
                planeDTO.getPlaneNumber(),
                planeDTO.getManufacturer(),
                planeDTO.getPlaneType(),
                planeDTO.getMaxSeats(),
                planeDTO.getMaxRange()
        );
    }
}
