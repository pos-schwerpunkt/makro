package at.itkolleg.planemanagement.api.mappers;

import at.itkolleg.planemanagement.domain.aggregate.Plane;
import at.itkolleg.planemanagement.shareddomain.model.PlaneResponseDTO;

public class EntityDTOMapper {
    public static PlaneResponseDTO toPlaneResponseDTO(Plane plane) {
        return new PlaneResponseDTO(
                plane.getPlaneNumber().getPlaneNumber(),
                plane.getManufacturer().getManufacturer(),
                plane.getPlaneType(),
                plane.getMaxSeats().getSeats(),
                plane.getMaxRange()
        );
    }
}
