package at.itkolleg.planemanagement.domain.aggregate;

import at.itkolleg.planemanagement.domain.commands.CreatePlaneCommand;
import at.itkolleg.planemanagement.domain.valueObjects.PlaneManufacturer;
import at.itkolleg.planemanagement.domain.valueObjects.PlaneNumber;
import at.itkolleg.planemanagement.domain.valueObjects.Seats;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotValidException;
import at.itkolleg.planemanagement.shareddomain.events.PlaneCreatedEvent;
import at.itkolleg.planemanagement.shareddomain.events.PlaneCreatedEventData;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@NoArgsConstructor
@EqualsAndHashCode
public class Plane extends AbstractAggregateRoot<Plane> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Embedded
    @Column(unique = true)
    @NotNull
    private PlaneNumber planeNumber;

    @NotNull
    @Embedded
    private PlaneManufacturer manufacturer;

    @NotNull
    private String planeType;

    @NotNull
    @Embedded
    private Seats maxSeats;

    @NotNull
    private double maxRange;

    public Plane(CreatePlaneCommand command) throws PlaneNumberNotValidException {
        this.planeNumber = new PlaneNumber(command.getPlaneNumber());
        this.manufacturer = new PlaneManufacturer(command.getManufacturer());
        this.planeType = command.getPlaneType();
        this.maxSeats = new Seats(command.getMaxSeats());
        this.maxRange = command.getMaxRange();

        addDomainEvent(
                new PlaneCreatedEvent(
                        new PlaneCreatedEventData(
                                this.planeNumber.getPlaneNumber(),
                                this.manufacturer.getManufacturer(),
                                this.planeType,
                                this.maxSeats.getSeats(),
                                this.maxRange
                        )
                )
        );
    }

    private void addDomainEvent(Object event) {
        registerEvent(event);
    }
}
