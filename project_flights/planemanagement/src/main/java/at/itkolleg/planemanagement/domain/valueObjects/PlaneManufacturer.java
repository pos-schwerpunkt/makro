package at.itkolleg.planemanagement.domain.valueObjects;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class PlaneManufacturer {
    private String manufacturer;

    protected PlaneManufacturer() {}

    public PlaneManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
