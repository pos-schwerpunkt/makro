package at.itkolleg.planemanagement.domain.valueObjects;

import at.itkolleg.planemanagement.exceptions.PlaneNumberNotValidException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class PlaneNumber {
    private String planeNumber;

    protected PlaneNumber() {}

    public PlaneNumber(String pNumber) throws PlaneNumberNotValidException {
        if (pNumber.matches("[A-Z]{1,2}[0-9]+[A-z]?")) {
            this.planeNumber = pNumber;
        } else {
            throw new PlaneNumberNotValidException();
        }

    }
}
