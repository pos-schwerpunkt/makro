package at.itkolleg.planemanagement.domain.valueObjects;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@EqualsAndHashCode
public class Seats {

    private int seats;

    protected Seats() {}

    public Seats(int seat) {
        this.seats = seat;
    }
}
