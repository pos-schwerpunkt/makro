package at.itkolleg.planemanagement.exceptions;

public class DuplicatePlaneNumberException extends Exception {
    public DuplicatePlaneNumberException() {
        super("Planenumber already in use!");
    }
}
