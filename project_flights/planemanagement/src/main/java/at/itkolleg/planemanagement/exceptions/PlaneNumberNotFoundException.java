package at.itkolleg.planemanagement.exceptions;

public class PlaneNumberNotFoundException extends Exception {
    public PlaneNumberNotFoundException() {
        super("Given planenumber not found in system.");
    }
}
