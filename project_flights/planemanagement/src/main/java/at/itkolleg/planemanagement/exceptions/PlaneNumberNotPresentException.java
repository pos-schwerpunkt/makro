package at.itkolleg.planemanagement.exceptions;

public class PlaneNumberNotPresentException extends Exception {
    public PlaneNumberNotPresentException() {
        super("Request Planenumber is not present.");
    }
}
