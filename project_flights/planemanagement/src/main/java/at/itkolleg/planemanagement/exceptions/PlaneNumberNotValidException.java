package at.itkolleg.planemanagement.exceptions;

public class PlaneNumberNotValidException extends Exception {
    public PlaneNumberNotValidException() {
        super("Plane number not valid.");
    }
}
