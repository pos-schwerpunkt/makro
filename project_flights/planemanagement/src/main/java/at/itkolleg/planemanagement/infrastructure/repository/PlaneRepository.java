package at.itkolleg.planemanagement.infrastructure.repository;

import at.itkolleg.planemanagement.domain.aggregate.Plane;
import at.itkolleg.planemanagement.domain.valueObjects.PlaneNumber;
import at.itkolleg.planemanagement.domain.valueObjects.Seats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlaneRepository extends JpaRepository<Plane, Long> {
    Optional<Plane> findPlaneByPlaneNumber(PlaneNumber number);
    List<Plane> findPlaneByMaxSeats(Seats seats);
    void deletePlanesByPlaneNumber(PlaneNumber number);
}
