package at.itkolleg.planemanagement.services;

import at.itkolleg.planemanagement.domain.aggregate.Plane;
import at.itkolleg.planemanagement.domain.commands.CreatePlaneCommand;
import at.itkolleg.planemanagement.domain.valueObjects.PlaneNumber;
import at.itkolleg.planemanagement.exceptions.DuplicatePlaneNumberException;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotPresentException;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotValidException;
import at.itkolleg.planemanagement.infrastructure.repository.PlaneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommandService {

    private final PlaneRepository repo;
    private final QueryService queryService;

    public String createPlane(CreatePlaneCommand command) throws DuplicatePlaneNumberException, PlaneNumberNotValidException {
        Plane plane = new Plane(command);
        if(queryService.PlaneNumberInUse(plane.getPlaneNumber().getPlaneNumber())) {
            throw new DuplicatePlaneNumberException();
        } else {
            repo.save(plane);
            return plane.getPlaneNumber().getPlaneNumber();
        }
    }

    public void deletePlane(String pNumber) throws PlaneNumberNotPresentException, PlaneNumberNotValidException {
        if(queryService.PlaneNumberInUse(pNumber)) {
            repo.deletePlanesByPlaneNumber(new PlaneNumber(pNumber));
        } else {
            throw new PlaneNumberNotPresentException();
        }
    }
}
