package at.itkolleg.planemanagement.services;

import at.itkolleg.planemanagement.infrastructure.messagebroker.PlaneChannels;
import at.itkolleg.planemanagement.shareddomain.events.PlaneCreatedEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
@AllArgsConstructor
@Slf4j
@EnableBinding(PlaneChannels.class)
public class DomainEventAndPublisherService {
    private final PlaneChannels planeChannels;

    @TransactionalEventListener
    public void handlePlaneCreatedEvent(PlaneCreatedEvent event) {
        try{
            log.info("Listend to domain event: Plane created: " + event.getEventData());
            log.info("Sending domain event to Message Broker Queue ... ");
            planeChannels.planeCreationSource().send(MessageBuilder.withPayload(event).build());
            log.info("Event has been sent successfully.");
        } catch (Exception e) {
            log.error("Custom Error: No connection to Message Broker established. Data not send: " + event.getEventData());
            e.printStackTrace();
        }
    }
}
