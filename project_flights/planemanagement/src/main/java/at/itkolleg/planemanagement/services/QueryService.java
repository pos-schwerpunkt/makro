package at.itkolleg.planemanagement.services;

import at.itkolleg.planemanagement.domain.aggregate.Plane;
import at.itkolleg.planemanagement.domain.valueObjects.PlaneNumber;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotPresentException;
import at.itkolleg.planemanagement.exceptions.PlaneNumberNotValidException;
import at.itkolleg.planemanagement.infrastructure.repository.PlaneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class QueryService {
    private final PlaneRepository repo;

    public boolean PlaneNumberInUse(String pNumber) throws PlaneNumberNotValidException {
        return repo.findPlaneByPlaneNumber(new PlaneNumber(pNumber)).isPresent();
    }

    public Plane getPlaneByPNumber(String pNumber) throws PlaneNumberNotPresentException, PlaneNumberNotValidException {
        Optional<Plane> optionalPlane = repo.findPlaneByPlaneNumber(new PlaneNumber(pNumber));
        if (optionalPlane.isPresent()) {
            return optionalPlane.get();
        } else {
            throw new PlaneNumberNotPresentException();
        }
    }

    public List<Plane> getAllPlanes() {
        return repo.findAll();
    }
}
