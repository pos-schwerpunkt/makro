package at.itkolleg.planemanagement.shareddomain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaneResponseDTO {
    private String planeNumber;
    private String manufacturer;
    private String planeType;
    private Integer maxSeats;
    private Double maxRange;
}
