########################### Project Flight Database Structure ###################################

drop database if exists bookingmsdb;
drop database if exists planemsdb;
drop database if exists passengermsdb;
drop database if exists flightmsdb;

create database bookingmsdb;
create database planemsdb;
create database passengermsdb;
create database flightmsdb;

## Let Spring generate those tables ;D
/*
################################# PassengerManagement-Microservice ###############################

use passengermsdb;
create table tbl_passenger(
id int NOT NULL auto_increment,
passNumber int NOT NULL,
firstName varchar(255) NOT NULL,
lastName varchar(255) NOT NULL,
dateOfBirth date NOT NULL,
email varchar(255) NOT NULL,
primary key(id)
)ENGINE=Innodb;

################################# PlaneManagement-Microservice ##################################

use planemsdb;
create table tbl_plane(
id int NOT NULL auto_increment,
planeNumber int NOT NULL,
manufacturer varchar(255) NOT NULL,
planeType varchar(255) NOT NULL,
maxSeats int NOT NULL,
maxRange double NOT NULL,
primary key(id)
)ENGINE=Innodb;

################################# FlightManagement-Microservice #################################

create table tbl_flight(
id int NOT NULL auto_increment,
planeNumber int NOT NULL,
passengers int NOT NULL,
expStart datetime NOT NULL,
expLanding datetime NOT NULL,
start varchar(255) NOT NULL,
landing varchar(255) NOT NULL,
primary key(id)
)ENGINE=Innodb;

################################# BookingManagement-Microservice ################################

create table tbl_booking(
id int NOT NULL auto_increment,
bookingNumber varchar(255) NOT NULL,
flightNumber varchar(255) NOT NULL,
passNumber varchar(255) NOT NULL,
price double NOT NULL,
paid boolean NOT NULL,
primary key(id)
)ENGINE=Innodb;

#################################################################################################
*/